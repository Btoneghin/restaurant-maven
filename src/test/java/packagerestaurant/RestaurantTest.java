package packagerestaurant;

/**
 * Created by Toneghin Baptiste on 26/11/2015.
 */



import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Ignore;

public class RestaurantTest {
	
	Commande c = new Commande();
	Entree e = new Entree("Salade", 5);
	Principal p = new Principal ("Viande", 5);
	Dessert d = new Dessert("Beignet", 5);
	Menu m = new Menu ("Complet", 10);
	
    @Test
    public void NePasPourvoirPayerAvantQueLaCommandeSoitEnEtatFinie() throws Exception {
    	c.setEtat(EtatCm.Finie);
		assertTrue(c.Encaisser());
    }
    
    @Test
    public void SommeMenuInferieurALaSommeDesPlatsALaCarte () throws Exception {
    	m.setEntree(e);
    	m.setPrincipal(p);
    	m.setDessert(d);
    	c.AjoutMenu(m);
    	
    	Commande c2 = new Commande();
    	c2.AjoutPlat(e);
    	c2.AjoutPlat(p);
    	c2.AjoutPlat(d);
    	
    	System.out.println(c.getSomme());
    	System.out.println(c2.getSomme());
    	assertTrue(c.getSomme() <= c2.getSomme());
    }
}
