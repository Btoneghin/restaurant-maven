package packagerestaurant;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class IHMTableCommander extends JFrame implements ActionListener {
	
    JButton cell = new JButton("Retour");
    JButton cell1 = new JButton("Table 1");
    JButton cell2 = new JButton("Table 2");
    JButton cell3 = new JButton("Table 3");
    JButton cell4 = new JButton("Table 4");
    JButton cell5 = new JButton("Table 5");
    
   	List<Table> tmp ;
    Restaurant r;

	public IHMTableCommander (Restaurant r) {
		this.r = r;
		
		cell.setPreferredSize(new Dimension (160,160));
	    cell1.setPreferredSize(new Dimension (160, 160));
	   	cell2.setPreferredSize(new Dimension (160, 160));
	   	cell3.setPreferredSize(new Dimension (160, 160));
	   	cell4.setPreferredSize(new Dimension (160, 160));
	   	cell5.setPreferredSize(new Dimension (160, 160));
	   	
	   	tmp = r.getLtable();
		
		//Cr�ation nouveaux �l�ments
		
		this.setTitle("Liste des tables");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    //Le conteneur principal
	    JPanel content = new JPanel();
	    content.setPreferredSize(new Dimension(600, 500));
	    content.setBackground(Color.WHITE);
	    //On d�finit le layout manager
	    this.setLayout(new GridBagLayout());
			
	    //L'objet servant � positionner les composants
	    GridBagConstraints gbc = new GridBagConstraints();
	    //On positionne la case de d�part du composant
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    //La taille en hauteur et en largeur
	    gbc.gridheight = 0;
	    gbc.gridwidth = 0;
	 
	   	content.add(cell1, gbc);
	   	
	   	gbc.gridx = gbc.gridx + 1;
	   	content.add(cell2,gbc);
	   	
	   	gbc.gridx = gbc.gridx + 1;
	   	content.add(cell3,gbc);
	   	
	   	gbc.gridx = gbc.gridx + 1;
	   	content.add(cell4,gbc);
	   	
	   	gbc.gridx = gbc.gridx + 1;
	   	content.add(cell5,gbc);

	    //---------------------------------------------
	    gbc.gridx = 1;
	    content.add(cell, gbc);
	    
	    //---------------------------------------------
	    //On ajoute le conteneur
	    this.setContentPane(content);
	    
	    cell1.addActionListener(this);
	    cell2.addActionListener(this);
	    cell3.addActionListener(this);
	    cell4.addActionListener(this);
	    cell5.addActionListener(this);
	    cell.addActionListener(this);
	    this.setVisible(true);	
	  }




	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == cell1) {
			this.setVisible(false);
			tmp.get(0).PrendreCommande (r);}
		if (source == cell2) {
			this.setVisible(false);
			tmp.get(1).PrendreCommande (r);}
		if (source == cell3) {
			this.setVisible(false);
			tmp.get(2).PrendreCommande (r);}
		if (source == cell4) {
			this.setVisible(false);
			tmp.get(3).PrendreCommande (r);}
		if (source == cell5) {
			this.setVisible(false);
			tmp.get(4).PrendreCommande (r);}
		if (source == cell) {
			this.setVisible(false);
			new IHMServeur(r);}
	}

}

