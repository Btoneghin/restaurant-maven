package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;



public class IHMServeur extends JFrame implements ActionListener {
    
	public Restaurant r;
	JButton cell1;
    JButton cell2;
    JButton cell3;

	public IHMServeur (Restaurant rest) {
		
		this.r = rest;

		
		//Cr�ation nouveaux �l�ments

		this.setTitle("Serveur");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    getContentPane().setLayout(null);
	    
	    cell1 = new JButton("Consulter Carte");
	    cell1.setBounds(25, 45, 150, 80);
	    getContentPane().add(cell1);
	    
	    cell2 = new JButton("Prendre Commande");
	    cell2.setBounds(215, 45, 150, 80);
	    getContentPane().add(cell2);
	    
	    cell3 = new JButton("Consulter Commande");
	    cell3.setBounds(400, 45, 150, 80);
	    getContentPane().add(cell3);
	    
	    Box boxEvent = Box.createVerticalBox();
	    boxEvent.setBounds(25, 177, 528, 174);
	    getContentPane().add(boxEvent);
	    
	    for(int i=0;i<r.getLtable().size();i++) {
	    	if (r.getLtable().get(i).getcommande() != null && r.getLtable().get(i).getcommande().getLticket() != null) {
		    	for (int j=0;j<r.getLtable().get(i).getcommande().getLticket().size();j++){
			    	if (r.getLtable().get(i).getcommande().getLticket().get(j).getEtat().getClass() == TicketTraite.class) {
			    		JLabel label = new JLabel ("Ticket pret pour la table " + r.getLtable().get(i).getNum());
			    		boxEvent.add(label);
			    	}
		    	}
	    	}
	    }
	    
	    cell1.addActionListener(this);
	    cell2.addActionListener(this);
	    cell3.addActionListener(this);
	    this.setVisible(true); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			Object source = arg0.getSource();
			if (source == cell1) {
				this.setVisible(false);
				new IHMCarte (r);}
			else if (source == cell2) {
				this.setVisible(false);
				new IHMTableCommander(r);}
			else if (source == cell3) {
				this.setVisible(false);
				new IHMTableTicket (r); }
		}
		

}
