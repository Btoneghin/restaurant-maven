package packagerestaurant;

public class TicketEtabli implements EtatTk{
	private Ticket ticket;
	
	public TicketEtabli(Ticket tk) {
		this.ticket = tk;
	}
	
	public void ChangeEtatTk() {
		ticket.setEtat( new TicketEnvoyer(ticket));
	}

	
}
