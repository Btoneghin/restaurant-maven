package packagerestaurant;

public class Boisson extends Element{
	
    private double quantite;
    
    public Boisson (String n, double p, double q){
    	super (n, p);
    	this.quantite = q;
    }


	public double getQuantite() {
		return quantite;
	}

	public void setQuantite(double q) {
		quantite = q;
	}
}
