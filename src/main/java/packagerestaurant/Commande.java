package packagerestaurant;

import java.util.ArrayList;
import java.util.List;

public class Commande {
	
	private List<Element> Lelement = new ArrayList<Element> ();
	private List<Element> LelementT = new ArrayList<Element> ();	
	private List<Menu> Lmenu = new ArrayList<Menu> ();
	private List<Ticket> Lticket = new ArrayList<Ticket> ();
	private double somme;
	private EtatCm etat;
	
	public Commande () {
		this.Lticket = null;
		this.etat = EtatCm.AttenteChoix;
		this.setSomme(0);
	}
	
	public void resteTicket () {
		for (int i=0;i<this.getLticket().size(); i++) {
			if (this.getLticket().get(i).getEtat().getClass() != TicketServi.class){
				this.etat = EtatCm.EnCours; }
			else { this.etat = EtatCm.Finie; }
			
		}
	}
	
	public boolean Encaisser() {
		if (this.getEtat() == EtatCm.Finie) {
			System.out.println("Commande payee");
			return true;
		}
		else {
			System.out.println("Impossible");
		}
		return false;
	}
	
	public Commande AjoutPlat (Element e) {
		this.Lelement.add(e);
		this.somme = this.somme + e.getPrix();
		return this;
	}
	
	public Commande AjoutMenu (Menu m) {
		this.Lmenu.add(m);
		this.somme = this.somme + m.getPrix();
		this.Lelement.add(m.getEntree());
		this.Lelement.add(m.getPrincipal());
		this.Lelement.add(m.getDessert());
		return this;
	}

	public List<Element> getLelement() {
		return Lelement;
	}

	public void setLelement(List<Element> lelement) {
		Lelement = lelement;
	}

	public List<Menu> getLmenu() {
		return Lmenu;
	}

	public void setLmenu(List<Menu> lmenu) {
		Lmenu = lmenu;
	}

	public List<Ticket> getLticket() {
		return Lticket;
	}

	public void setLticket(List<Ticket> lticket) {
		Lticket = lticket;
	}

	public List<Element> getLelementT() {
		return LelementT;
	}

	public void setLelementT(List<Element> lelementT) {
		LelementT = lelementT;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(int somme) {
		this.somme = somme;
	}

	public EtatCm getEtat() {
		return etat;
	}

	public void setEtat(EtatCm etat) {
		this.etat = etat;
	}
}
