package packagerestaurant;

public class Table {

	private int num;
	private Commande c;
	
	public Table (int i) {
		this.num = i;
		this.c = null;
	}
	


	public void CreerCommande (Table t) {
		this.c = new Commande ();
	}
	
	public void PrendreCommande(Restaurant r) {
		CreerCommande(this);
		new IHMCommande (r, this);
	}
	
	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
			this.num = num;
	}
	
	public Commande getcommande() {
		return c;
	}

	public void setcommande(Commande c) {
		this.c = c;
	}
	



}
