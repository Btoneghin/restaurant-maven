package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IHMMjour extends JFrame implements ActionListener {
	
    JButton cell = new JButton("Retour");
    Restaurant r;

	public IHMMjour (Restaurant r) {
		this.r = r;

		
		//Cr�ation nouveaux �l�ments
		
		this.setTitle("Liste des menus");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    //On d�finit le layout manager
	    Container content = getContentPane();
	    Box V1 = Box.createVerticalBox();
	    Box H1 = Box.createHorizontalBox();
	    Box H2 = Box.createHorizontalBox();
	    
	    H2.add(Box.createGlue());
	    H2.add(cell);

		for (int i=0; i<r.getCarte().getMenu().size(); i++ ) {
			if (r.getCarte().getMenu().get(i).getNom() == "Jour" ) {
			    V1.add(new JLabel("                         Menu du " + r.getCarte().getMenu().get(i).getNom() + " : " + String.valueOf(r.getCarte().getMenu().get(i).getPrix()) + " �"));
			}
		}

	    V1.add(new JLabel ("       Entree :"));
	    V1.add(new JLabel ("             " + r.getCarte().getElement().get(0).getNom()));
	    V1.add(new JLabel ("       Plat :"));
	    V1.add(new JLabel ("             " + r.getCarte().getElement().get(4).getNom()));
	    V1.add(new JLabel ("       Dessert :"));
	    V1.add(new JLabel ("             " + r.getCarte().getElement().get(8).getNom()));
	    
	    
	    this.getContentPane().add(V1, BorderLayout.WEST);
	    this.getContentPane().add(H1, BorderLayout.NORTH);
	    this.getContentPane().add(H2, BorderLayout.SOUTH);
	    //On ajoute le conteneur
	    this.setContentPane(content);
	    
	    cell.addActionListener(this);
	    this.setVisible(true);	
	  }



	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == cell) {
			this.setVisible(false);
			new IHMCarte(r);}
	}


}

