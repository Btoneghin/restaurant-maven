package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JInternalFrame;


public class IHMCommande extends JFrame implements ActionListener{
	
	JButton btnEntree= new JButton("Entree");
	JButton btnPrincipal = new JButton("Principal");
	JButton btnDessert = new JButton("Dessert");
	JButton btnMenu = new JButton("Menu");
	JButton btnBoisson = new JButton("Boisson");
	JButton Ret = new JButton("Fermer");
	JButton Ret2 = new JButton("Fermer");
	JButton Ret3 = new JButton("Fermer");
	JButton Ret4 = new JButton("Fermer");
	JButton Ret5 = new JButton("Fermer");
	
	JButton Back = new JButton("Retour");
	JButton Next = new JButton("Terminer");
	
	JButton E[];
	Entree Ee[];
	JButton P[];
	Principal Pe[];
	JButton D[];
	Dessert De [];
	JButton B[];
	Boisson Be [];
	JButton M[];
	Menu Me[];

	JInternalFrame InterEntree = new JInternalFrame("Entree");
	JInternalFrame InterPrincipal = new JInternalFrame("Principal");
	JInternalFrame InterDessert = new JInternalFrame("Dessert");
	JInternalFrame InterBoisson = new JInternalFrame("Boisson");
	JInternalFrame InterMenu = new JInternalFrame("Menu");
	Restaurant r;
	Table t;

	public IHMCommande(Restaurant r, Table tbl) {
		this.r = r;
		this.t = tbl;

		this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
		
		E = new JButton [r.getCarte().getEntree().size()];
		Ee = new Entree [r.getCarte().getEntree().size()];
		P = new JButton [r.getCarte().getPrincipal().size()];
		Pe = new Principal [r.getCarte().getPrincipal().size()];
		D = new JButton [r.getCarte().getDessert().size()];
		De = new Dessert [r.getCarte().getDessert().size()];
		B = new JButton [r.getCarte().getBoisson().size()];
		Be = new Boisson [r.getCarte().getBoisson().size()];
		M = new JButton [r.getCarte().getMenu().size()];
		Me = new Menu [r.getCarte().getMenu().size()];

		btnEntree.setBounds(10, 148, 100, 50);
		this.getContentPane().add(btnEntree);
		
		btnPrincipal.setBounds(10, 212, 100, 50);
		this.getContentPane().add(btnPrincipal);
		
		btnDessert.setBounds(10, 273, 100, 50);
		this.getContentPane().add(btnDessert);
		
		btnMenu.setBounds(10, 26, 100, 50);
		this.getContentPane().add(btnMenu);
		
		btnBoisson.setBounds(10, 87, 100, 50);
		this.getContentPane().add(btnBoisson);
		
		Back.setBounds(300, 312, 100, 50);
		this.getContentPane().add(Back);
		
		Next.setBounds(484, 312, 100, 50);
		this.getContentPane().add(Next);
		
		
		Ret.setBounds(326, 283, 100, 50);
		Ret2.setBounds(326, 283, 100, 50);
		Ret3.setBounds(326, 283, 100, 50);
		Ret4.setBounds(326, 283, 100, 50);
		Ret5.setBounds(326, 283, 100, 50);
		
		
//		--------------------------------------------------------------------------------------------------
		
		InterMenu.setBounds(142, 0, 442, 362);
		this.getContentPane().add(InterMenu);
		InterMenu.getContentPane().setLayout(null);
		
		int h = 0;
		int w = 20;		
		for (int i = 0; i<M.length; i++){
			M[i] = new JButton (r.getCarte().getMenu().get(i).getNom());
			Me[i] = new Menu (r.getCarte().getMenu().get(i).getNom(), r.getCarte().getMenu().get(i).getPrix());
			Me[i].setEntree(r.getCarte().getMenu().get(i).getEntree());
			Me[i].setPrincipal(r.getCarte().getMenu().get(i).getPrincipal());
			Me[i].setDessert(r.getCarte().getMenu().get(i).getDessert());
			if (h*100+20<300) {
				M[i].setBounds(w, h*100+20, 100, 50);
				h = h+1; }
			else {
				h=0;
				w = w+135;
				M[i].setBounds(w, h*100+20, 100, 50);
			}
			InterMenu.getContentPane().add(M[i]);
		}
		

		InterMenu.getContentPane().add(Ret);
		
		InterMenu.setVisible(false);
		
//		--------------------------------------------------------------------------------------------------
		

		InterBoisson.setBounds(142, 0, 442, 362);
		this.getContentPane().add(InterBoisson);
		InterBoisson.getContentPane().setLayout(null);
		
		h=0;
		w = 20;
		for (int i = 0; i<B.length; i++){
			B[i] = new JButton (r.getCarte().getBoisson().get(i).getNom());
			Be[i] = new Boisson (r.getCarte().getBoisson().get(i).getNom(), r.getCarte().getBoisson().get(i).getPrix(), ((Boisson) r.getCarte().getBoisson().get(i)).getQuantite ());
			if (h*100+20<300) {
				B[i].setBounds(w, h*100+20, 100, 50);
				h = h+1; }
			else {
				h=0;
				w = w+135;
				B[i].setBounds(w, h*100+20, 100, 50);
			}
			InterBoisson.getContentPane().add(B[i]);
		}
		
		InterBoisson.getContentPane().add(Ret2);
		
		InterBoisson.setVisible(false);
		
//		------------------------------------------------------------------------------------------------
		

		InterEntree.setBounds(142, 0, 442, 362);
		this.getContentPane().add(InterEntree);
		InterEntree.getContentPane().setLayout(null);
		
		h = 0;
		w = 20;
		for (int i = 0; i<E.length; i++){
			E[i] = new JButton (r.getCarte().getEntree().get(i).getNom());
			Ee[i] = new Entree (r.getCarte().getEntree().get(i).getNom(), r.getCarte().getEntree().get(i).getPrix());
			if (h*100+20<300) {
				E[i].setBounds(w, h*100+20, 100, 50);
				h = h+1; }
			else {
				h=0;
				w = w+135;
				E[i].setBounds(w, h*100+20, 100, 50);
			}
			InterEntree.getContentPane().add(E[i]);
		}
		
		InterEntree.getContentPane().add(Ret3);
		
		InterEntree.setVisible(false);
		
//		--------------------------------------------------------------------------------------------------
		

		InterPrincipal.setBounds(142, 0, 442, 362);
		this.getContentPane().add(InterPrincipal);
		InterPrincipal.getContentPane().setLayout(null);
		
		h = 0;
		w = 20;
		for (int i = 0; i<P.length; i++){
			P[i] = new JButton (r.getCarte().getPrincipal().get(i).getNom());
			Pe[i]= new Principal (r.getCarte().getPrincipal().get(i).getNom(), r.getCarte().getPrincipal().get(i).getPrix());
			if (h*100+20<300) {
				P[i].setBounds(w, h*100+20, 100, 50);
				h = h+1; }
			else {
				h=0;
				w = w+135;
				P[i].setBounds(w, h*100+20, 100, 50);
			}
			InterPrincipal.getContentPane().add(P[i]);
		}
		
		InterPrincipal.getContentPane().add(Ret4);
		
		InterPrincipal.setVisible(false);
//------------------------------------------------------------------------------------------------

		InterDessert.setBounds(142, 0, 442, 362);
		this.getContentPane().add(InterDessert);
		InterDessert.getContentPane().setLayout(null);
		
		h = 0;
		w = 20;
		for (int i = 0; i<D.length; i++){
			D[i] = new JButton (r.getCarte().getDessert().get(i).getNom());
			De[i] = new Dessert (r.getCarte().getDessert().get(i).getNom(), r.getCarte().getDessert().get(i).getPrix());
			if (h*100+20<300) {
				D[i].setBounds(w, h*100+20, 100, 50);
				h = h+1; }
			else {
				h=0;
				w = w+135;
				D[i].setBounds(w, h*100+20, 100, 50);
			}
			InterDessert.getContentPane().add(D[i]);
		}
		
		InterDessert.getContentPane().add(Ret5);
		
		InterDessert.setVisible(false);
		
//     -----------------------------------------------------------------------------------------------------
		
		JInternalFrame Inter = new JInternalFrame("Inter");
		Inter.setBounds(142, 0, 442, 362);
		this.getContentPane().add(Inter);
		Inter.getContentPane().setLayout(null);
		
		Inter.setVisible(false);
//		------------------------------------------------------------------------------------------------
		
		btnEntree.addActionListener(this);		
		btnPrincipal.addActionListener(this);	
		btnDessert.addActionListener(this);	
		btnMenu.addActionListener(this);	
		btnBoisson.addActionListener(this);
		Ret.addActionListener(this);
		Ret2.addActionListener(this);
		Ret3.addActionListener(this);
		Ret4.addActionListener(this);
		Ret5.addActionListener(this);
		Back.addActionListener(this);
		Next.addActionListener(this);

		addAction(B);
		addAction(E);
		addAction(P);
		addAction(D);
		addAction(M);
		
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == btnEntree) {
			InterEntree.setVisible(true);
			InterPrincipal.setVisible(false);
			InterDessert.setVisible(false);
			InterMenu.setVisible(false);
			InterBoisson.setVisible(false);
			Back.setVisible(false);
			Next.setVisible(false);}
		if (source == btnPrincipal) {
			InterEntree.setVisible(false);
			InterPrincipal.setVisible(true);
			InterDessert.setVisible(false);
			InterMenu.setVisible(false);
			InterBoisson.setVisible(false);
			Back.setVisible(false);
			Next.setVisible(false);}
		if (source == btnDessert) {
			InterEntree.setVisible(false);
			InterPrincipal.setVisible(false);
			InterDessert.setVisible(true);
			InterMenu.setVisible(false);
			InterBoisson.setVisible(false);
			Back.setVisible(false);
			Next.setVisible(false);}
		if (source == btnMenu) {
			InterEntree.setVisible(false);
			InterPrincipal.setVisible(false);
			InterDessert.setVisible(false);
			InterMenu.setVisible(true);
			InterBoisson.setVisible(false);
			Back.setVisible(false);
			Next.setVisible(false);}
		if (source == btnBoisson) {
			InterEntree.setVisible(false);
			InterPrincipal.setVisible(false);
			InterDessert.setVisible(false);
			InterMenu.setVisible(false);
			InterBoisson.setVisible(true);
			Back.setVisible(false);
			Next.setVisible(false);}
		if (source == Ret || source == Ret2 || source == Ret3 || source == Ret4 || source == Ret5 ) {
			InterEntree.setVisible(false);
			InterPrincipal.setVisible(false);
			InterDessert.setVisible(false);
			InterMenu.setVisible(false);
			InterBoisson.setVisible(false);
			Back.setVisible(true);
			Next.setVisible(true);}
		if (source == Back) {
			this.setVisible(false);
			t.setcommande(new Commande ());
			new IHMServeur (r) ; }
		if (source == Next) {
			this.setVisible(false);
			new IHMServeur (r);
			
		}

		addPerformedE(E, source, Ee);
		addPerformedE(P, source, Pe);
		addPerformedE(D, source, De);
		addPerformedE(B, source, Be);
		addPerformedM(M, source, Me);
	}

	public void addPerformedE (JButton j[], Object src, Element e[]) {
		for (int i = 0; i<j.length; i++){
			if (src == j[i]) {
				t.getcommande().AjoutPlat(e[i]);
			}
		}
	}
	
	public void addPerformedM (JButton j[], Object src, Menu m[]) {
		for (int i = 0; i<j.length; i++){
			if (src == j[i]) {
				t.getcommande().AjoutMenu(m[i]);
			}
		}
	}

	public void addAction (JButton j[]) {
		for (int i = 0; i<j.length; i++){
			j[i].addActionListener(this);
		}
	}
	
}
