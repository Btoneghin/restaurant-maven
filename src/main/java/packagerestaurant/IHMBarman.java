package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class IHMBarman extends JFrame implements ActionListener {

	Restaurant r;
	JButton refresh;
	List<Table> LT ;
	JButton T[];
	int Ind[];

	public IHMBarman(Restaurant r) {
		this.r = r;
		LT = r.getLtable();
		T = new JButton [LT.size()];
		Ind = new int [T.length];
				
		this.setTitle("Barman");
		this.setBounds(50, 50, 600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		refresh = new JButton("Actualiser");
		refresh.setBounds(495, 339, 89, 23);
		getContentPane().add(refresh);
		refresh.addActionListener(this);
		
		int x = 0;
		int y = 0;
		int z;

		for (int i = 0; i<LT.size(); i++) {
			if (LT.get(i).getcommande() != null && LT.get(i).getcommande().getLticket() != null) {
				for (int j=0; j<LT.get(i).getcommande().getLticket().size(); j++) {
					if (LT.get(i).getcommande().getLticket().get(j).getLelement().get(0).getClass() == Boisson.class) {
						if (LT.get(i).getcommande().getLticket().get(j).getEtat().getClass() == TicketEnvoyer.class) {
							Ind[i] = j;
							if (x+100 < 400) {
								x = x + 100; }
							else {
								x = 0;
								y = y + 50;
								x = x + 100; }
							z = i + 1;
							T[i] = new JButton ("Table " + z);
							T[i].setBounds(x+25, y+10, 100, 50);
							getContentPane().add(T[i]);
							T[i].addActionListener(this);
							T[i].setVisible(true);
						}
					}
				}
			}
		}

		this.setVisible(true);
	}


	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		addPerformedE (T, source, Ind);
		if (source == refresh) {
			this.setVisible(false);
			new IHMBarman (r); }
	}
	
	public void addPerformedE (JButton j[], Object src, int ind[]) {
		for (int i = 0; i<j.length; i++){
			if (src == j[i]) {
				LT.get(i).getcommande().getLticket().get(ind[i]).changeEtatTk();
				this.setVisible(false);
				new IHMBarman (r);
				new IHMAffichageTicket (LT.get(i).getcommande().getLticket().get(ind[i]));
			}
		}
	}
}
