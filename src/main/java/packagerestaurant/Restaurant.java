package packagerestaurant;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    
	public Carte carte;
    private List<Table> Ltable = new ArrayList<Table> ();
    private List<Ticket> LTicket = new ArrayList<Ticket> ();

	public Restaurant () {
    	this.carte = new Carte ();
    	
    	Table T1 = new Table (1);
    	Table T2 = new Table (2);
    	Table T3 = new Table (3);
    	Table T4 = new Table (4);
    	Table T5 = new Table (5);
    	this.Ltable.add(T1);
    	this.Ltable.add(T2);
    	this.Ltable.add(T3);
    	this.Ltable.add(T4);
    	this.Ltable.add(T5);
    	
    	
		List<Entree> Lentree = new ArrayList<Entree> ();
		List<Principal> Lprincipal = new ArrayList<Principal> ();
		List<Dessert> Ldessert = new ArrayList<Dessert> ();
		
		Entree e1 = new Entree ("Salade", 5);
		Entree e2 = new Entree ("Charcuterie", 5);
		Entree e3 = new Entree ("Tomate", 5);
		
		this.getCarte().getElement().add(e1);
		this.getCarte().getElement().add(e2);
		this.getCarte().getElement().add(e3);
		Lentree.add(e1);
		Lentree.add(e2);
		Lentree.add(e3);
		
    	Principal p1 = new Principal ("Viande", 10);
        Principal p2 = new Principal ("Pizza", 10);
    	Principal p3 = new Principal ("Pates", 10);
    	Principal p4 = new Principal ("Poisson", 10);
    	
    	this.getCarte().getElement().add(p1);
    	this.getCarte().getElement().add(p2);
    	this.getCarte().getElement().add(p3);
    	this.getCarte().getElement().add(p4);
    	Lprincipal.add(p4);
    	Lprincipal.add(p1);
    	
		Dessert d1 = new Dessert ("Glace", 4);
		Dessert d2 = new Dessert ("Creme Brulee", 4);
		Dessert d3 = new Dessert ("Beignet", 4);
		
		this.getCarte().getElement().add(d1);
		this.getCarte().getElement().add(d2);
		this.getCarte().getElement().add(d3);
		Ldessert.add(d1);
		Ldessert.add(d2);
		Ldessert.add(d3);
    	
    	Boisson b1 = new Boisson ("Biere", 5, 50);
    	Boisson b2 = new Boisson ("Biere", 3, 33);
    	Boisson b3 = new Boisson ("Coca", 3, 33);
    	Boisson b4 = new Boisson ("Vin", 3, 25);
    	this.getCarte().getElement().add(b1);
    	this.getCarte().getElement().add(b2);
    	this.getCarte().getElement().add(b3);
    	this.getCarte().getElement().add(b4);
    	
    	Menu m1 = new Menu ("Viande", 15);
    	Menu m2 = new Menu ("Poisson", 15);
		Entree em1 = new Entree ("Salade", 0);
		Entree em2 = new Entree ("Charcuterie", 0);
    	Principal pm1 = new Principal ("Viande", 0);
        Principal pm2 = new Principal ("Poisson", 0);
		Dessert dm1 = new Dessert ("Glace", 0);
		Dessert dm2 = new Dessert ("Creme Brulee", 0);
    	m1.setEntree((Element)em1);
    	m2.setEntree((Element)em2);
    	m1.setPrincipal((Element)pm1);
    	m2.setPrincipal((Element)pm2);
    	m1.setDessert((Element)dm1);
    	m2.setDessert((Element)dm2);
    	this.getCarte().getMenu().add(m1);
    	this.getCarte().getMenu().add(m2);
    	
    }

    
	public Carte getCarte() {
		return carte;
	}
	
	public void setCarte(Carte carte) {
		this.carte = carte;
	}
	

	public List<Table> getLtable() {
		return Ltable;
	}

	public void setLtable(List<Table> ltable) {
		Ltable = ltable;
	}


	public List<Ticket> getLTicket() {
		return LTicket;
	}


	public void setLTicket(List<Ticket> lTicket) {
		LTicket = lTicket;
	}

}
