package packagerestaurant;

public class TicketEnvoyer implements EtatTk{
	private Ticket ticket;
	
	public TicketEnvoyer(Ticket tk) {
		this.ticket = tk;
	}
	
	public void ChangeEtatTk() {
		ticket.setEtat( new TicketEnPreparation (ticket));
	}

	
}
