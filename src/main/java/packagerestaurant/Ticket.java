package packagerestaurant;

import java.util.ArrayList;
import java.util.List;

public class Ticket {
	
	private int num;
	private EtatTk etat;
	private List<Element> Lelement = new ArrayList<Element> ();
	
	public Ticket() {

	}
	
	public Ticket AjoutElement(Commande c, Element e) {
		this.Lelement.add(e);
		c.getLelementT().add(e);
		for (int i = 0; i<c.getLelement().size(); i++) {
			if (c.getLelement().get(i) == e) {
				c.getLelement().remove(i);
				break;
			}
		}
		return this;
	}
	
	public void changeEtatTk () {
		this.etat.ChangeEtatTk();
	}
	
	
	

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public List<Element> getLelement() {
		return Lelement;
	}

	public void setLelement(List<Element> lelement) {
		Lelement = lelement;
	}

	public EtatTk getEtat() {
		return etat;
	}

	public void setEtat(EtatTk etat) {
		this.etat = etat;
	}




}
