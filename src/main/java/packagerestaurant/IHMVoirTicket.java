package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import java.awt.BorderLayout;

public class IHMVoirTicket extends JFrame implements ActionListener {

	Restaurant r;
	Commande c;
	JButton ret;
	
	Box b[];
	JButton j[];

	
    
	public IHMVoirTicket(Restaurant r, Commande c) {
		this.r = r;
		this.c = c;
		
		j = new JButton [c.getLticket().size()];
		b = new Box [c.getLticket().size()];
		this.setTitle("Liste Ticket : ");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    JPanel panel = new JPanel();
	    getContentPane().add(panel, BorderLayout.CENTER);
	    panel.setLayout(null);
	    
	    ret = new JButton("Retour");
	    ret.setBounds(495, 339, 89, 23);
	    
	    int x = 20;
	    int y = 10;
	    for (int i=0;i<b.length;i++) {
	    	JLabel tLabel = new JLabel ("Ticket " + i + " : " + c.getLticket().get(i).getEtat().toString());
	    	tLabel.setBounds(x,y,120,14);
	    	panel.add(tLabel);
	    	x = x+145;
	    	if (x+145 >600) { x = 20; y = 185; }
	    }
	    
	    x = 35;
	    y = 35;
	    for (int i=0;i<b.length;i++) {
	    	b[i] = Box.createVerticalBox();
	    	b[i].setBounds(x, y, 80, 120);
	    	x = x+140;
	    	if (x+140 >600) { x = 35; y = 210; }
	    	panel.add(b[i]);
	    }

	    for (int i=0;i<j.length;i++) {
	    	for (int j=0;j<c.getLticket().get(i).getLelement().size();j++) {
	    		JLabel el = new JLabel (c.getLticket().get(i).getLelement().get(j).getNom());
	    		b[i].add(el);
	    	}
	    }
	    
	    for (int i=0;i<j.length;i++) {
	    	if (c.getLticket().get(i).getEtat().getClass() == TicketEtabli.class) {
	    		j[i] = new JButton ("Envoyer"); 
	    		j[i].addActionListener(this);
		    	j[i].setVisible(true);
		    	b[i].add(j[i]);
		    	}
	    	else if (c.getLticket().get(i).getEtat().getClass() == TicketTraite.class) {
	    		j[i] = new JButton ("Servi"); 
	    		j[i].addActionListener(this); 
		    	j[i].setVisible(true); 
		    	b[i].add(j[i]);}
	    	else {
	    		j[i] = null;
	    	}
	    }
	    
	    panel.add(ret);
	    
	    ret.addActionListener(this);
	    this.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == ret) {
			this.setVisible(false);
			new IHMServeur(r);
		}
		ActionPerformed (j, source);
	}
	
	public void ActionPerformed (JButton j[], Object src) {
		for (int i=0;i<j.length;i++) {
			if (src == j[i]) {
				if (c.getLticket().get(i).getEtat().getClass() == TicketEtabli.class) {
					c.getLticket().get(i).changeEtatTk();
					this.setVisible(false);
					new IHMVoirTicket (r,c);
				}
				if (c.getLticket().get(i).getEtat().getClass() == TicketTraite.class) {
					c.getLticket().get(i).changeEtatTk();
					this.setVisible(false);
					new IHMVoirTicket (r,c);
				}
			}
		}
	}
}
