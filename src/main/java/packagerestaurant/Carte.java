package packagerestaurant;


import java.util.ArrayList;
import java.util.List;


public class Carte {
	
    private List<Menu> Lmenu = new ArrayList<Menu> ();
    private List<Element> Lelement = new ArrayList<Element> ();

	public Carte () {
		
    }
	
	
	public List<Element> getPrincipal () {
		List<Element> tmp = new ArrayList<Element> ();
		for (int i = 0; i<this.getElement().size(); i++) {
   	 		if ( this.getElement().get(i).getClass() == Principal.class ) {
   	 			tmp.add(this.getElement().get(i)); }
		}
		return tmp;
	}
	
	public List<Element> getEntree () {
		List<Element> tmp = new ArrayList<Element> ();
		for (int i = 0; i<this.getElement().size(); i++) {
   	 		if ( this.getElement().get(i).getClass() == Entree.class ) {
   	 			tmp.add(this.getElement().get(i)); }
		}
		return tmp;
	}
	
	public List<Element> getDessert () {
		List<Element> tmp = new ArrayList<Element> ();
		for (int i = 0; i<this.getElement().size(); i++) {
   	 		if ( this.getElement().get(i).getClass() == Dessert.class ) {
   	 			tmp.add(this.getElement().get(i)); }
		}
		return tmp;
	}
	
	public List<Element> getBoisson () {
		List<Element> tmp = new ArrayList<Element> ();
		for (int i = 0; i<this.getElement().size(); i++) {
   	 		if ( this.getElement().get(i).getClass() == Boisson.class ) {
   	 			tmp.add(this.getElement().get(i)); }
		}
		return tmp;
	}
	
	public List<Element> getElement() {
		return Lelement ;
	}
	
	public void setElement(List<Element> element) {
		this.Lelement = element;
	}
	
    public List<Menu> getMenu() {
		return Lmenu;
	}

	public void setMenu(List<Menu> menu) {
		this.Lmenu = menu;
	}


}