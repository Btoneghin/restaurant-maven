package packagerestaurant;

public class Element {
	
	private String Nom;
	private double Prix;
	
	public Element (String s, double d) {
	    this.Nom = s;
	    this.Prix = d;
	}
	
	public String getNom() {
		return Nom;
	}
	 
	public void setNom(String nom) {
		Nom = nom;
	}

	public double getPrix() {
		return Prix;
	}
	
	public void setPrix(double prix) {
		Prix = prix;
	}
}
