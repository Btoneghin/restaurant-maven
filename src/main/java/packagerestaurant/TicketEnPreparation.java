package packagerestaurant;

public class TicketEnPreparation implements EtatTk {

	private Ticket ticket;
	
	public TicketEnPreparation(Ticket tk) {
		this.ticket = tk;
	}
	
	public void ChangeEtatTk() {
		ticket.setEtat( new TicketTraite (ticket));
	}

	
}

