package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class IHMCaissier extends JFrame implements ActionListener {

	Restaurant r;
	JButton refresh;
	List<Table> LT ;
	JButton T[];
	int Ind[];

	public IHMCaissier(Restaurant r) {
		this.r = r;
		LT = r.getLtable();
		T = new JButton [LT.size()];
		Ind = new int [LT.size()];
		
		this.setTitle("Caissier");
		this.setBounds(1300, 350, 600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		refresh = new JButton("Actualiser");
		refresh.setBounds(484, 312, 100, 50);
		getContentPane().add(refresh);
		
		int x = 25;
		int y = 25;
		int z;
		for (int i=0;i<LT.size(); i++) {
			if (LT.get(i).getcommande() != null) {
				z = i+1;
				Ind[i] = i;
				T[i] = new JButton ("Table " + z);
				T[i].setBounds(x, y, 100, 50);
				T[i].addActionListener(this);
				getContentPane().add(T[i]);
				if (x+125 < 400) {
					x = x + 125; }
				else {
					x = 0;
					y = y + 100;
					x = x + 125; }
			}
		}	
		
		refresh.addActionListener(this);
		this.setVisible(true);
	}


	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		addPerformedE (T, source, Ind);
		if (source == refresh) {
			this.setVisible(false);
			new IHMCaissier (r); }
	}
	
	public void addPerformedE (JButton j[], Object src, int ind[]) {
		for (int i = 0; i<j.length; i++){
			if (src == j[i]) {
				this.setVisible(false);
				new IHMAffichageCaissier(r, LT.get(ind[i]));
			}
		}
	}
}
