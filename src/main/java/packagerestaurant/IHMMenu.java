package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;


import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class IHMMenu extends JFrame implements ActionListener {
	
    JButton cell = new JButton("Retour");
    Restaurant r;
    
	public IHMMenu (Restaurant r) {
		this.r = r;
		//Cr�ation nouveaux �l�ments
		
		this.setTitle("Liste des menus");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    //On d�finit le layout manager
	    this.setLayout(new BorderLayout());
	    Box V1 = Box.createVerticalBox();
	    Box V2 = Box.createVerticalBox();
	    Box V3 = Box.createVerticalBox();
	    Box H = Box.createHorizontalBox();
	    H.add(Box.createGlue());
	    H.add(cell);
	    
	    List<Menu> tmp = r.getCarte().getMenu();	    
   	    
	    this.getContentPane().add(PresentationMenu(tmp.get(0), V1), BorderLayout.LINE_START);
	    this.getContentPane().add(PresentationMenu(tmp.get(1), V2), BorderLayout.LINE_END);
//	    this.getContentPane().add(PresentationMenu(tmp.get(2), V3), BorderLayout.LINE_END);
	    this.getContentPane().add(H, BorderLayout.SOUTH);

	    

	    cell.addActionListener(this);
	    this.setVisible(true);	
	  }


	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == cell) {
			this.setVisible(false);
			new IHMCarte(r);}
	}

	
	public Box PresentationMenu (Menu m, Box b) {		
		b.add(add(new JLabel (m.getNom()+ " : "+ m.getPrix()+ "�")));
		b.add(add(new JLabel ("Entrees : " + m.getEntree().getNom())));
		b.add(add(new JLabel ("Plats : " + m.getPrincipal().getNom())));
		b.add(add(new JLabel ("Dessert : " + m.getDessert().getNom())));
		return b;
	}





}

