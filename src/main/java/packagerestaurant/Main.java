package packagerestaurant;

public class Main {

	public static void main(String[] args) {
		Restaurant r = new Restaurant ();
		new IHMServeur (r);
		new IHMBarman (r);
		new IHMCuisinier (r);
		new IHMCaissier(r);
	}
}
