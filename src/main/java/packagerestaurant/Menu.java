package packagerestaurant;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    
	public double Prix;
    public String Nom;

    private Element ent, pr, des;

	public Menu (String n, double p) {
    	this.Nom = n;
    	this.Prix = p;
    }
    
    public double getPrix() {
		return Prix;
	}

	public void setPrix(double prix) {
		Prix = prix;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

    public Element getEntree() {
		return ent;
	}

	public void setEntree(Element ent) {
		this.ent = ent;
	}

	public Element getPrincipal() {
		return pr;
	}

	public void setPrincipal(Element pr) {
		this.pr = pr;
	}

	public Element getDessert() {
		return des;
	}

	public void setDessert(Element des) {
		this.des = des;
	}


}
