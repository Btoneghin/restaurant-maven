package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import java.awt.BorderLayout;


public class IHMVoirCommande extends JFrame implements ActionListener {
	
    JButton ret ;
    JButton tck;
    JButton ticketbutton;
    Restaurant r;
    Table t;
    List<Element>tmpe;
    List<Element>tmpt;
    JRadioButton c;
    
	JRadioButton E[];
	Element Ee[];
	JRadioButton P[];
	Element Pe[];
	JRadioButton D[];
	Element De [];
	JRadioButton B[];
	Element Be [];
	
	
	Ticket tk;

    
	public IHMVoirCommande (Restaurant r, Table t) {
		this.r = r;
		this.t = t;
		this.tmpe = t.getcommande().getLelement();
		this.tmpt = t.getcommande().getLelementT();
		
		int e=0, p=0, d=0, b=0;
		for (int i = 0; i<tmpe.size(); i++) {
			if (tmpe.get(i).getClass() == Entree.class) {
				e = e+1;}
			if (tmpe.get(i).getClass() == Principal.class) {
				p = p+1;}
			if (tmpe.get(i).getClass() == Dessert.class) {
				d = d+1;}
			if (tmpe.get(i).getClass() == Boisson.class) {
				b = b+1;
			}
		}
		
		
		E = new JRadioButton [e];
		Ee = new Element [e];
		P = new JRadioButton [p];
		Pe = new Element [p];
		D = new JRadioButton [d];
		De = new Element [d];
		B = new JRadioButton [b];
		Be = new Element [b];
		
				
		this.setTitle("Commande : ");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    JPanel panel = new JPanel();
	    getContentPane().add(panel, BorderLayout.CENTER);
	    panel.setLayout(null);
	    
	    JLabel ent = new JLabel("Entree");
	    ent.setBounds(45, 28, 46, 14);
	    panel.add(ent);
	    
	    JLabel pri = new JLabel("Principal");
	    pri.setBounds(180, 28, 46, 14);
	    panel.add(pri);
	    
	    JLabel des = new JLabel("Dessert");
	    des.setBounds(330, 28, 46, 14);
	    panel.add(des);
	    
	    JLabel boi = new JLabel("Boisson");
	    boi.setBounds(474, 28, 46, 14);
	    panel.add(boi);
	    
	    Box vent = Box.createVerticalBox();
	    vent.setBounds(10, 55, 111, 250);
	    panel.add(vent);

	    
	    Box vpri = Box.createVerticalBox();
	    vpri.setBounds(147, 55, 111, 250);
	    panel.add(vpri);
	    
	    Box vdes = Box.createVerticalBox();
	    vdes.setBounds(297, 55, 111, 250);
	    panel.add(vdes);
	    
	    Box vboi = Box.createVerticalBox();
	    vboi.setBounds(442, 55, 111, 250);
	    panel.add(vboi);
	    
	    ret = new JButton("Retour");
	    ret.setBounds(0, 339, 89, 23);
	    panel.add(ret);
	    
	    tck = new JButton("Envoyer");
	    tck.setBounds(495, 339, 89, 23);
	    panel.add(tck);
	    
	    if (t.getcommande().getLticket() != null) { 
		    ticketbutton = new JButton("Liste ticket");
		    ticketbutton.setBounds(229, 339, 111, 23);
		    panel.add(ticketbutton);
			ticketbutton.addActionListener(this);
	    }
	    
	    boolean be=false, bp=false, bd=false, bb=false;
	    int ne=0, np=0, nd=0, nb=0;
	    // Creation composant
	    for (int i =0; i<tmpe.size(); i++) {
		    JRadioButton rdb = new JRadioButton(tmpe.get(i).getNom());
	    	if (tmpe.get(i).getClass() == Entree.class) {
	    		if (be == false) { be = true;}
	    		else {ne = ne+1;}
	    	    E[ne] = rdb;
	    	    Ee[ne] = tmpe.get(i);
	    	    vent.add(E[ne]);
	    	}
	    	if (tmpe.get(i).getClass() == Principal.class) {
	    		if (bp == false) { bp = true;}
	    		else {np = np+1;}
	    	    P[np] = rdb;
	    	    Pe[np] = tmpe.get(i);
	    	    vpri.add(P[np]);
	    	}
	    	if (tmpe.get(i).getClass() == Dessert.class) {
	    		if (bd == false) { bd = true;}
	    		else {nd = nd+1;}
	    	    D[nd] = rdb;
	    	    De[nd] = tmpe.get(i);
	    		vdes.add(D[nd]);
	    	}
	    	if (tmpe.get(i).getClass() == Boisson.class) {
	    		if (bb == false) { bb = true;}
	    		else {nb = nb+1;}
	    	    B[nb] = rdb;
	    	    Be[nb] = tmpe.get(i);
	    		vboi.add(B[nb]);
	    	}
	    }

		
	    for (int i =0; i<tmpt.size(); i++) {
		    JLabel rdb = new JLabel(tmpt.get(i).getNom());
	    	if (tmpt.get(i).getClass() == Entree.class) {
	    	    vent.add(rdb); }
	    	if (tmpt.get(i).getClass() == Principal.class) {
	    	    vpri.add(rdb); }
	    	if (tmpt.get(i).getClass() == Dessert.class) {
	    	    vdes.add(rdb); }
	    	if (tmpt.get(i).getClass() == Boisson.class) {
	    	    vboi.add(rdb); }
	    }
	    
		ret.addActionListener(this);
		tck.addActionListener(this);
		this.setVisible(true);

	}
	

	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == ret) {
			this.setVisible(false);
			new IHMServeur(r);}
		if (source == ticketbutton) {
			this.setVisible(false);
			new IHMVoirTicket(r, t.getcommande());
		}
		CreerTicket (source,E,P,D,B,Ee,Pe,De,Be);



	}
	
	public void CreerTicket (Object source,
			JRadioButton j[], JRadioButton j2[], JRadioButton j3[], JRadioButton j4[],
			Element e[], Element e2[], Element e3[], Element e4[]) {
		if (source == tck) {
			Ticket tk = new Ticket ();
			tk.setEtat(new TicketEtabli(tk));
			selection(j,e,tk);
			selection(j2,e2,tk);
			selection(j3,e3,tk);
			selection(j4,e4,tk);
			if (t.getcommande().getLticket() == null) {
				t.getcommande().setLticket(new ArrayList<Ticket> ());
				t.getcommande().getLticket().add(tk);
			}
			else { 
				t.getcommande().getLticket().add(tk);	
			}
			this.setVisible(false);
			new IHMVoirCommande (r, t);}
	}
	
	public void selection (JRadioButton j[], Element e[], Ticket tk) {
		for (int i = 0; i<j.length; i++){
			if (j[i].isSelected()) {
				tk.AjoutElement(t.getcommande(), e[i]);
			}
		}
	}
}
