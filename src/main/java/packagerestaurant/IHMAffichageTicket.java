package packagerestaurant;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JButton;

public class IHMAffichageTicket extends JFrame implements ActionListener{
	
	Ticket t;
    JButton refresh;
    
	public IHMAffichageTicket(Ticket t) {
		this.t = t;
		
		//Cr�ation nouveaux �l�ments
		
		this.setTitle("Ticket");
	    this.setSize(250, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    getContentPane().setLayout(null);
	    
	    Box vB = Box.createVerticalBox();
	    vB.setBounds(10, 11, 183, 309);
	    getContentPane().add(vB);
	    
	    refresh = new JButton("Terminer");
	    refresh.setBounds(145, 339, 89, 23);
	    getContentPane().add(refresh);
	    refresh.addActionListener(this);
		
		for (int i=0; i<t.getLelement().size(); i++) {
			if (t.getLelement().get(i).getClass() == Boisson.class) {
				JLabel j = new JLabel (t.getLelement().get(i).getNom() + " (" + ((Boisson) t.getLelement().get(i)).getQuantite() + "cl)");
				vB.add(j);
			}
			else  {
				JLabel j = new JLabel (t.getLelement().get(i).getNom());
				vB.add(j);
			}
		};
		this.setVisible(true);
		
	}


	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == refresh) {
			this.setVisible(false);
			t.changeEtatTk();
		}
	}
}
