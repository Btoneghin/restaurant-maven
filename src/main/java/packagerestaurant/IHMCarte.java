package packagerestaurant;

import javax.swing.JFrame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class IHMCarte extends JFrame implements ActionListener{
	
    JButton cell1 = new JButton("Menus");
    JButton cell2 = new JButton("Boissons");
    JButton cell3 = new JButton("Entrees");
    JButton cell4 = new JButton("Plats");
    JButton cell5 = new JButton("Desserts");
    JButton cell6 = new JButton("Retour");

    
    Restaurant r;
    
	public IHMCarte (Restaurant r) {
		this.r = r;
		
		cell1.setPreferredSize(new Dimension (160,160));
		cell2.setPreferredSize(new Dimension (160,160));
		cell3.setPreferredSize(new Dimension (160,160));
		cell4.setPreferredSize(new Dimension (160,160));
		cell5.setPreferredSize(new Dimension (160,160));
		cell6.setPreferredSize(new Dimension (160,160));
		
		//Cr�ation nouveaux �l�ments

		this.setTitle("Consulter la carte");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    //Le conteneur principal
	    JPanel content = new JPanel();
	    content.setPreferredSize(new Dimension(600, 500));
	    content.setBackground(Color.WHITE);
	    //On d�finit le layout manager
	    this.setLayout(new GridBagLayout());
			
	    //L'objet servant � positionner les composants
	    GridBagConstraints gbc = new GridBagConstraints();
	   	 			
	    //On positionne la case de d�part du composant
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    //La taille en hauteur et en largeur
	    gbc.gridheight = 0;
	    gbc.gridwidth = 0;
	    content.add(cell1, gbc);
	    //---------------------------------------------
	    gbc.gridx = 1;
	    content.add(cell2, gbc);
	    //---------------------------------------------
	    gbc.gridx = 2;		
	    content.add(cell3, gbc);		

	    //---------------------------------------------
	    gbc.gridx = 0;
	    gbc.gridy = 1;
	    content.add(cell4, gbc);
	    //---------------------------------------------
	    gbc.gridx = 1;
	    content.add(cell5, gbc);
	    //---------------------------------------------	    
	    gbc.gridx = 1;
	    content.add(cell6, gbc);

	    //---------------------------------------------
	    
	    
	    //On ajoute le conteneur
	    this.setContentPane(content);

	    
	    cell1.addActionListener(this);
	    cell2.addActionListener(this);
	    cell3.addActionListener(this);
	    cell4.addActionListener(this);
	    cell5.addActionListener(this);
	    cell6.addActionListener(this);

	    this.setVisible(true);	
	  }


	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == cell1) {
			this.setVisible(false);
			new IHMMenu(r);}
		if (source == cell2) {
			this.setVisible(false);
			new IHMBoisson(r);}
		if (source == cell3) {
			this.setVisible(false);
			new IHMEntree(r);}
		if (source == cell4) {
			this.setVisible(false);
			new IHMPlat(r);}
		if (source == cell5) {
			this.setVisible(false);
			new IHMDessert(r);}
		if (source == cell6) {
			this.setVisible(false);
			new IHMServeur(r);}
	}



}
