package packagerestaurant;

import javax.swing.JFrame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

	
public class IHMBoisson extends JFrame implements ActionListener {

    JButton cell = new JButton("Retour");
	Restaurant r;

	public IHMBoisson (Restaurant r) {
		this.r = r;
		
		cell.setPreferredSize(new Dimension (160,160));
		
		//Cr�ation nouveaux �l�ments
		
		this.setTitle("Liste des boissons");
	    this.setSize(600, 400);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    //Le conteneur principal
	    JPanel content = new JPanel();
	    content.setPreferredSize(new Dimension(600, 500));
	    content.setBackground(Color.WHITE);
	    //On d�finit le layout manager
	    this.setLayout(new GridBagLayout());
			
	    //L'objet servant � positionner les composants
	    GridBagConstraints gbc = new GridBagConstraints();
	   	 			
	    //On positionne la case de d�part du composant
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    //La taille en hauteur et en largeur
	    gbc.gridheight = 0;
	    gbc.gridwidth = 0;
	    
	    List<Element> tmp = r.getCarte().getBoisson();
	    
	    for (int i = 0; i<tmp.size(); i++) {
   	 		JButton b = new JButton(tmp.get(i).getNom() + " (" + ((Boisson) tmp.get(i)).getQuantite() + "cl) : " + String.valueOf(tmp.get(i).getPrix()) + " �");
   	 		b.setPreferredSize(new Dimension (160, 160));
   	 		content.add(b, gbc);
   	 		gbc.gridx = gbc.gridx + 1 ;	 	
	    }
	    //---------------------------------------------
	    gbc.gridx = 1;
	    content.add(cell, gbc);
	    
	    //---------------------------------------------
	    //On ajoute le conteneur
		this.setContentPane(content);
		    
		cell.addActionListener(this);
		this.setVisible(true);	
	  }


	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == cell) {
			this.setVisible(false);
			new IHMCarte(r);}
	}

}

