package packagerestaurant;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.Box;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class IHMAffichageCaissier extends JFrame implements ActionListener {

	Restaurant r;
	JButton ret;
	JButton payer;
	Table t;

	public IHMAffichageCaissier(Restaurant r, Table t) {
		this.r = r;
		this.t = t;
		Commande c = t.getcommande();
		
		this.setTitle("Affichage Commande");
		this.setBounds(1300, 350, 600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

	    
	    JLabel ent = new JLabel("Entree");
	    ent.setBounds(170, 28, 46, 14);
	    getContentPane().add(ent);
	    
	    JLabel pri = new JLabel("Principal");
	    pri.setBounds(275, 28, 46, 14);
	    getContentPane().add(pri);
	    
	    JLabel des = new JLabel("Dessert");
	    des.setBounds(406, 28, 46, 14);
	    getContentPane().add(des);
	    
	    JLabel boi = new JLabel("Boisson");
	    boi.setBounds(515, 28, 46, 14);
	    getContentPane().add(boi);
	    
	    Box vent = Box.createVerticalBox();
	    vent.setBounds(137, 53, 111, 200);
	    getContentPane().add(vent);

	    
	    Box vpri = Box.createVerticalBox();
	    vpri.setBounds(258, 53, 101, 200);
	    getContentPane().add(vpri);
	    
	    Box vdes = Box.createVerticalBox();
	    vdes.setBounds(369, 53, 119, 200);
	    getContentPane().add(vdes);
	    
	    Box vboi = Box.createVerticalBox();
	    vboi.setBounds(498, 53, 76, 200);
	    getContentPane().add(vboi);
	    
	    ret = new JButton("Retour");
	    ret.setBounds(0, 312, 100, 50);
	    getContentPane().add(ret);
	    
	    JLabel solde = new JLabel("Somme � payer : " + c.getSomme() + " �");
	    solde.setBounds(228, 302, 200, 14);
	    getContentPane().add(solde);
	    
	    payer = new JButton("Payer");
	    payer.setBounds(484, 312, 100, 50);
	    getContentPane().add(payer);
	    
	    c.resteTicket();
	    JLabel comm = new JLabel("Etat de la commande : " + c.getEtat().toString());
	    comm.setBounds(202, 277, 226, 14);
	    getContentPane().add(comm);
	    
	    Box vmenu = Box.createVerticalBox();
	    vmenu.setBounds(10, 53, 111, 200);
	    getContentPane().add(vmenu);
	    
	    JLabel menu = new JLabel("Menu");
	    menu.setBounds(39, 28, 46, 14);
	    getContentPane().add(menu);
	    
	    for (int i=0;i<c.getLmenu().size();i++) {
	    	 JLabel lb = new JLabel(c.getLmenu().get(i).getNom() + " : " + c.getLmenu().get(i).getPrix() + " �");
	    	 vmenu.add(lb);
	    }
	    

	    for (int i =0; i<c.getLelementT().size(); i++) {
		    JLabel lb = new JLabel(c.getLelementT().get(i).getNom() + " : " + c.getLelementT().get(i).getPrix() + " �");
	    	if (c.getLelementT().get(i).getClass() == Entree.class) {
	    	    vent.add(lb);
	    	}
	    	if (c.getLelementT().get(i).getClass() == Principal.class) {
	    	    vpri.add(lb);
	    	}
	    	if (c.getLelementT().get(i).getClass() == Dessert.class) {
	    		vdes.add(lb);
	    	}
	    	if (c.getLelementT().get(i).getClass() == Boisson.class) {
	    		vboi.add(lb);
	    	}
	    }
	    
	    for (int i =0; i<c.getLelement().size(); i++) {
		    JLabel lb = new JLabel(c.getLelement().get(i).getNom() + " : " + c.getLelement().get(i).getPrix() + " �");
	    	if (c.getLelement().get(i).getClass() == Entree.class) {
	    	    vent.add(lb);
	    	}
	    	if (c.getLelement().get(i).getClass() == Principal.class) {
	    	    vpri.add(lb);
	    	}
	    	if (c.getLelement().get(i).getClass() == Dessert.class) {
	    		vdes.add(lb);
	    	}
	    	if (c.getLelement().get(i).getClass() == Boisson.class) {
	    		vboi.add(lb);
	    	}
	    }
	    
	    if (c.getEtat() == EtatCm.Finie) {
	    	payer.addActionListener(this);
	    }
		ret.addActionListener(this);
		this.setVisible(true);
	}


	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == ret) {
			this.setVisible(false);
			new IHMCaissier (r); }
		if (source == payer) {
			this.setVisible(false);
			t.setcommande(null);	
			new IHMCaissier(r);	}
	}
}